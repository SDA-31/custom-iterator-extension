use std::iter;

mod transform;

use crate::transform::{TransformExt, TransformType};

fn main() {
    let n = 10_001;

    let mut number = 1;

    use TransformType::*;
    let a = iter::repeat_with(|| {
        number += 1;

        number
    })
    .transform(vec![], |i, acc| {
        if acc.iter().find(|&prime| i % prime == 0).is_some() {
            return Skip;
        }

        let mut vec = acc.to_owned();
        vec.push(i);

        Result(i, vec)
    })
    .take(n)
    .last()
    .unwrap();

    println!("{:?}", a);
}

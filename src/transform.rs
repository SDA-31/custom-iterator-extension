use std::cell::{RefCell, Ref};

pub struct Transform<I, P, Acc>
where
    I: Iterator,
    Acc: Sized + Clone,
    P: FnMut(I::Item, Ref<Acc>) -> TransformType<I::Item, Acc>,
{
    iter: I,
    predicate: P,
    acc: RefCell<Acc>
}

#[allow(dead_code)]
pub enum TransformType<I, Acc>
where
    I: Sized,
    Acc: Sized + Clone,
{
    Skip,
    Halt,
    Result(I, Acc),
}

impl<I, Acc, P> Iterator for Transform<I, P, Acc>
where
    I: Iterator,
    Acc: Sized + Clone,
    P: FnMut(I::Item, Ref<Acc>) -> TransformType<I::Item, Acc>,
{
    type Item = I::Item;

    fn next(&mut self) -> Option<Self::Item> {
        use TransformType::*;
        let mut item = None;
        while let Some(x) = self.iter.next() {
            match (self.predicate)(x, self.acc.borrow()) {
                Skip => continue,
                Halt => break,
                Result(i, accum) => {
                    self.acc = RefCell::new(accum);
                    item = Some(i);
                    break;
                }
            }
        }
        return item;
    }
}

pub trait TransformExt: Iterator {
    fn transform<Acc, P>(self, init: Acc, f: P) -> Transform<Self, P, Acc>
    where
        Acc: Sized + Clone,
        Self: Sized,
        P: FnMut(Self::Item, Ref<Acc>) -> TransformType<Self::Item, Acc>,
    {
        Transform {
            iter: self,
            predicate: f,
            acc: RefCell::new(init),
        }
    }
}

impl<I: Iterator> TransformExt for I {}
